#SCAN FOR BLOBS
import cv2
import math
import numpy as np
import gopigo as go
from gopigo import *
import time
go.set_speed(120)
frames = 0
fps = 0
i=0
starttime = time.gmtime()
startsecs = starttime[5]


#defining functions for movements
def forward(t):
    go.fwd()
    time.sleep(t)
    go.stop()

def right(t):
    go.right()
    time.sleep(t)
    go.stop()

def left(t):
    go.left()
    time.sleep(t)
    go.stop()
    
def back(t):
    go.bwd()
    time.sleep(t)
    go.stop()

def human_close():
    if kaugus > 90 and kaugus < 120:
        return True
    else:
        return False

def human_close():
    if secs - timer_time == 3:
        return False
    if kaugus > 60 and kaugus < 200:
        timer_time = secs
        return True



    


blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.minArea = 500
blobparams.maxArea = 500000
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False
detector = cv2.SimpleBlobDetector_create(blobparams)



cap = cv2.VideoCapture(0)
cv2.namedWindow("Original")

lH = 0
lS = 195
lV = 130
hH = 180
hS = 255
hV = 255
lowerLimits = np.array([lH, lS, lV])
upperLimits = np.array([hH, hS, hV])




    #functions to change values
def update_lH(new_value):
    global lH
    global lowerLimits
    lH = new_value
    lowerLimits = np.array([lH, lS, lV])
    return

def update_lS(new_value):
    global lS
    global lowerLimits
    lS = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_lV(new_value):
    global lV
    global lowerLimits
    lV = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_hH(new_value):
    global hH
    global upperLimits
    hH = new_value
    upperLimits = np.array([hH, hS, hV])
    return

def update_hS(new_value):
    global hS
    global upperLimits
    hS = new_value
    upperLimits = np.array([hH, hS, hV])
    return


def update_hV(new_value):
    global hV
    global upperLimits
    hV = new_value
    upperLimits = np.array([hV, hS, hV])
    return
'''
#create window for trackbars and put them there
cv2.namedWindow("Trackbars")
cv2.createTrackbar("lH", "Trackbars", lH, 255, update_lH)
cv2.createTrackbar("lS", "Trackbars", lS, 255, update_lS)
cv2.createTrackbar("lV", "Trackbars", lV, 255, update_lV)
cv2.createTrackbar("hH", "Trackbars", hH, 255, update_hH)
cv2.createTrackbar("hS", "Trackbars", hS, 255, update_hS)
cv2.createTrackbar("hV", "Trackbars", hV, 255, update_hV)
'''

while True:
    #read the image from the camera

    realtime = time.gmtime()
    realsecs = realtime[5]
        
    secs = realsecs - startsecs


    ret, frame = cap.read()
    
    #convert bgr to hsv picture
    #frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #r = [len(frame)-220, len(frame)-150, 0, len(frame[0])]
    
    #frame=frame[r[0]:r[1], r[2]:r[3]]
    
    
    #blurred = cv2.GaussianBlur(frame, (3,3), 0)

    #thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    #outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    #ret, thresh = cv2.threshold(outimage, 5, 255, cv2.THRESH_BINARY)
    #thresh = cv2.bitwise_not(thresh)
    #kernel = np.ones((7,7),np.uint8)
    #closed = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    #keypoints = detector.detect(thresh)
    #newimg = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
   
   #convert bgr to hsv picture
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    r = [len(frame)-220, len(frame)-150, 0, len(frame[0])]
    
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    
    #detected keypoints are there
    keypoints = detector.detect(outimage)
   
    


   #keypoint scan
    for keypoint in keypoints:
         cv2.putText(thresholded, str(int(round(keypoint.pt[0], 1))) + " " + str(int(round(keypoint.pt[1], 1))), (int(keypoint.pt[0]), int(keypoint.pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (27, 255, 27), 2)
    
        
    thresholded = cv2.drawKeypoints(thresholded, keypoints, np.array([]), (0,255,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


    #NO BLOBS
    if len(keypoints) < 1:
        
    #F6
        right(0.1)
        
    
    
    #BLOB DETECTED
    else:
        
        
        #F8, ALIGNING WITH BLOB CENTER
        
        blob_center = keypoints[0].pt[0]

        # if human_close() is True:
        #     go.stop()
        #     sleep(0.1)

        if blob_center <= 260:
            left(0.1)
            print('KEERAN VASAKULE')
        
        if blob_center >= 380:
            right(0.1)
            print('KEERAN PAREMALE')

        #movement 
        if blob_center < 380 and blob_center > 260:
            forward(0.2)
            print('EDASI JU')


    cv2.imshow('Original', thresholded)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    


# When everything done, release the capture
print ('closing program')
cap.release()
cv2.destroyAllWindows()





   
    
    