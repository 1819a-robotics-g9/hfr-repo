#import numpy as np 
#import cv2
import gopigo as go
import math
import time

#BreadBoardi jaoks
GPIO.setmode(GPIO.BOARD)

#Ultrasonic sensor
Go_Trigger = 5 #pin 
Go_Echo = 4 #pin

#PIR sensor
#Pir_Pin = #pin

GPIO.setup(Go_Trigger,GPIO.OUT)  
GPIO.setup(Go_Echo,GPIO.IN) 


def ultrasonic(Go_Trigger,Go_Echo):
      start=0
      stop=0
      # Set pins as output and input
      GPIO.setup(Go_Trigger,GPIO.OUT)  # Trigger
      GPIO.setup(Go_Echo,GPIO.IN)      # Echo
     
      # Set trigger to False (Low)
      GPIO.output(Go_Trigger, False)
     
      # Allow module to settle
      time.sleep(0.01)
           
      #while distance > 5:
      #Send 10us pulse to trigger
      GPIO.output(Go_Trigger, True)
      time.sleep(0.00001)
      GPIO.output(Go_Trigger, False)
      begin = time.time()
      while GPIO.input(Go_Echo)==0 and time.time()<begin+0.05:
            start = time.time()
     
      while GPIO.input(Go_Echo)==1 and time.time()<begin+0.1:
            stop = time.time()
     
      # Calculate pulse length
      elapsed = stop-start
      # Distance pulse travelled in that time is time
      # multiplied by the speed of sound (cm/s)
      distance = elapsed * 34000
     
      # That was the distance there and back so halve the value
      distance = distance / 2
     
      print ("Distance : %.1f" % distance)
      # Reset GPIO settings
      return distance


