import numpy as np
import cv2
import time
import gopigo as go


# Open the camera
cap = cv2.VideoCapture(0)

#filter limits
bilateral = 75
blur = 1
#for light dots removal parameters
kernel = np.ones((5,5), np.uint8)
# colour detection limits
lH = 40
lS = 45
lV = 68
hH = 86
hS = 255
hV = 220
lowerLimits = np.array([lH, lS, lV])
upperLimits = np.array([hH, hS, hV])

state = True

#functions to change values
def update_lH(new_value):
    global lH
    global lowerLimits
    lH = new_value
    lowerLimits = np.array([lH, lS, lV])
    return

def update_lS(new_value):
    global lS
    global lowerLimits
    lS = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_lV(new_value):
    global lV
    global lowerLimits
    lV = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_hH(new_value):
    global hH
    global upperLimits
    hH = new_value
    upperLimits = np.array([hH, hS, hV])
    return

def update_hS(new_value):
    global hS
    global upperLimits
    hS = new_value
    upperLimits = np.array([hH, hS, hV])
    return

def update_hV(new_value):
    global hV
    global upperLimits
    hV = new_value
    upperLimits = np.array([hV, hS, hV])
    return

def update_bilateral(new_value):
    global bilateral
    bilateral = new_value
    return

def update_blur(new_value):
    global blur
    if new_value%2 == 0:
        blur = new_value+1
    return


cv2.namedWindow("Trackbars")
cv2.createTrackbar("lH", "Trackbars", lH, 255, update_lH)
cv2.createTrackbar("lS", "Trackbars", lS, 255, update_lS)
cv2.createTrackbar("lV", "Trackbars", lV, 255, update_lV)
cv2.createTrackbar("hH", "Trackbars", hH, 255, update_hH)
cv2.createTrackbar("hS", "Trackbars", hS, 255, update_hS)
cv2.createTrackbar("hV", "Trackbars", hV, 255, update_hV)


blobparams = cv2.SimpleBlobDetector_Params()

blobparams.filterByArea = True
blobparams.filterByArea = True
blobparams.maxArea = 90000
blobparams.minArea = 50
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False


#defining blob detector
detector = cv2.SimpleBlobDetector_create(blobparams)

#Set robot speed range(0-255)
go.set_speed(25)
#go.stop()
kestvus = True


#otsimine alguses
def otsi():
    go.right_rot()

#liigub otse, kui 2 posti leitud
def liigu():
    go.forward()
    #kui 1 post, siis hakkab otsima
    if len(keypoints) <= 1:
        go.left_rot()
    #kui 2 posti, hakkab liikuma otse
    if len(keypoints) == 2:
        x1 = int(keypoints[0].pt[0])
        #kui postid on 22rtes, siis hakkab liikuma otse ja paneb programmi kinni
        if x1 < 180 and x1 > 360:
            
            go.set_left_speed(50)
            go.set_right_speed(50)
            go.forward()
            go.stop()
            kestvus = False
            go.stop()

        #arvutab kahe posti vahe
        #kui on yle poole  v alla poole, siis hakkab vastavas suunas liikuma
        if x1 < 300:
            go.right()
        elif x1 > 340:
            go.left()

while kestvus:   
    # Read the image from the camera
    ret, frame = cap.read()
    
    r = [len(frame)-220, len(frame)-190, 0, len(frame[0])]
    
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    #convert bgr to hsv picture
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Thresholded operations
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    outimage = cv2.bitwise_not(outimage)

    #kirjutab thresholded pildi ylemise ja alumise 22re yle,
    #et ta tuvastaks ka 22re l2hedalt
    thresholded[0] = 255
    thresholded[len(thresholded)-1] = 255
        
    #using bilaterial filter, keeps edges sharp 
    blurred = cv2.bilateralFilter(outimage, 9, bilateral, bilateral)
    
    #yks filter korraga, muidu tombab fpsi alla
    #using gaussian blur filter
    #blurred = cv2.blur(outimage, (blur, blur))
    
            
    #using opening morphology, in this case deletes light dots on the ball
    blurred = cv2.morphologyEx(blurred, cv2.MORPH_OPEN, kernel)
    
    #detected keypoints are there
    keypoints = detector.detect(thresholded)
        
    
     #siin saab teada, kas kaks posti on tuvastatud, kui ei,
    #l2heb otsi funktsiooni
    if len(keypoints) < 1 and state == True:
        otsi()
    else:
        #kui kaks posti on tehtud, hakkab t2itma liigu funktsiooni
        state = False
        go.stop()
        liigu()
    
    #make circle around detected item
    frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    
    
    # Show this image on a window named "Original"
    cv2.imshow('Original', frame)
    cv2.imshow('Filter', thresholded)

    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print 'closing program'
go.stop()
cap.release()
cv2.destroyAllWindows()
