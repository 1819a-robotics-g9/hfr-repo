﻿### HUMAN FOLLOWING ROBOT ###

Our team:

* Egert Ojamäe, Human Following Robot

* Rudolf Põldma, Human Following Robot

* Silver Kotter, Human Following Robot

* Virko Värnik, Human Following Robot


### Component list ###

| Component	    | Quantity| School kit|
| ----------------- |:-------:| ---------:|
| GoPiGo2           |    1    |    yes    |
| RaspberryPi 3     |    1    |    yes    |
| PIR		    |    1    |    yes    |
| Ultrasonic sensor |    2    |    yes    |
| Battery	    |    1    |    yes    |
| Wires		    |    1    |    yes    |
| Breadboard	    |    1    |    yes    |
| Camera	    |    1    |    yes    |


### Team 1. Image processing ###
*Silver Kotter, Virko Värnik*

Our team is responsible for image processing. We are going to use OpenCV to analyze live video feed from a camera, which is in turn connected to a Raspberry Pi. We are going to use an OpenCV  object detection method to detect human shapes. Using this method is called using Haar cascades. Our primary goal is to detect a human shape and return appropriate values to other parts of the code. This includes the size of the human shape and the location of it on the screen. Using this in conjunction with our PIR and US sensors we can pinpoint the location of a human and make the robot follow them. 

* Week 11 - Haar cascade test code, optimizing to use it with school issued camera.

* Week 13 - Human detection code configured for current project

* Week 14 - Human and object detection is fully optimized, coordinates of the human shape are returned as variables. 

* Week 15 - Integration of image processing and robot movement code.


### Team 2. Robot control ###
*Egert Ojamäe, Rudolf Põldma*

Our team is responsible for image processing. We are going to use OpenCV to analyze live video feed from a camera, which is in turn connected to a Raspberry Pi. We are going to use an OpenCV  object detection method to detect human shapes. Using this method is called using Haar cascades. Our primary goal is to detect a human shape and return appropriate values to other parts of the code. This includes the size of the human shape and the location of it on the screen. Using this in conjunction with our PIR and US sensors we can pinpoint the location of a human and make the robot follow them. We will use the PIR sensor for activating the robot when the human it follows starts to move. We aim to use 2x ultrasonic sensors, one for keeping the desired distance from the human it follows, and the other one to detect any possible obsticles in its way. 

* Week 11 - Using a ultrasonic sensor, make the robot stay between the desired distance of 0.5-1(m).

* Week 13 - Figure out the logic of how to get the robot to follow a certain person using the PIR and ultrasonic sensor.

* Week 15 - Using the second ultrasonic sensor to effectively avoid obsticles in its path.



### Project overview ###

Our team project is a robot using the GoPiGo platform and sensors to track human and follow from a distance of 50-100cm. The robot will have two wheels, a base for the components, a Raspberry Pi along with a GoPiGo module. It will use OpenCV image processing and object detection to see human silhouettes to see where it should turn and drive to. It will also use a passive infrared sensor to double check if said object is indeed a human (detecting body temp). Two ultrasonic sensors will also be mounted on the robot. One of them will be pointed 60 degrees upwards to see the distance of detected humans once they are in the middle of the robots detection field. Another ultrasonic sensor will be mounter in front of the robot to detect obstacles and help avoid them. 

Using the feedback from the OpenCV image processing, the PIR sensor and the Ultrasonic sensor, the robot will scan for human, and if it has sighted a potential human shape, it will turn to it and drive toward it, maintaining a distance of 50-100cm in doing so. If image processing detects a image but the PIR does not detect an appropriate temperature, the image will be classified as an error and disregarded. The robot will then scan again and upon detecting a new possible human emitting IR radiation, it will lock on to that and follow it again. Upon detecting an obstacle close to it by the frontal US sensor, the robot will try to turn around the obstacle. If it fails to do so it will reset and start scanning for a human again.

### Challenges and Solutions ###
Challenges:

* We had a robot, whose right wheel did not work properly and slowed down our work.
* Ultrasonic sensor gives us sometimes wrong data. 

Solutions:

* Next time we will check our robot's technical condition.
* We discussed that we can use camera instead of an ultrasonic sensor to measure distance.

