// Datasheet for Ultrasonic Ranging Module HC - SR04
// https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf

int echoPin = 6;
int trigPin = 5;
int delay_us = 60;  
long distance_mm = 0;
long duration_us = 10;
// defining row and column pins for ease of use.
int R1 = 2; //DotMtrx 9
int R2 = 3; //DotMtrx 14
int R3 = 4; //DotMtrx 8
int R4 = 9; //DotMtrx 12
int R5 = 10; //DotMtrx 1
int R6 = 7; //DotMtrx 7
int R7 = 8; //DotMtrx 2
int C1 = A0; //DotMtrx 13
int C2 = A1; //DotMtrx 3
int C3 = A2; //DotMtrx 4
int C4 = A3; //DotMtrx 10
int C5 = A4; //DotMtrx 6
int t = 55;

void setup()  {
  Serial.begin(9600);
  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);
    pinMode(R1, OUTPUT);
  pinMode(R2, OUTPUT);
  pinMode(R3, OUTPUT);
  pinMode(R4, OUTPUT);
  pinMode(R5, OUTPUT);
  pinMode(R6, OUTPUT);
  pinMode(R7, OUTPUT);
  pinMode(C1, OUTPUT);
  pinMode(C2, OUTPUT);
  pinMode(C3, OUTPUT);
  pinMode(C4, OUTPUT);
  pinMode(C5, OUTPUT);

// for starters turning all the LED's off in the matrix
  digitalWrite(R1, LOW);
  digitalWrite(R2, LOW);
  digitalWrite(R3, LOW);
  digitalWrite(R4, LOW);
  digitalWrite(R5, LOW);
  digitalWrite(R6, LOW);
  digitalWrite(R7, LOW);
  digitalWrite(C1, HIGH);
  digitalWrite(C2, HIGH);
  digitalWrite(C3, HIGH);
  digitalWrite(C4, HIGH);
  digitalWrite(C5, HIGH);
 
}

void vilgu(){
    // put your main code here, to run repeatedly:
  digitalWrite(C3, LOW);
  digitalWrite(C2,LOW);
  digitalWrite(R1, HIGH);
  delay(t);
  digitalWrite(R1, LOW);
  digitalWrite(R2, HIGH);
  delay(t);
  digitalWrite(R2, LOW);
  digitalWrite(R3, HIGH);
  delay(t);
  digitalWrite(R3, LOW);
  digitalWrite(R4, HIGH);
  delay(t);
  digitalWrite(R4, LOW);
  digitalWrite(R5, HIGH);
  delay(t);
  digitalWrite(R5, LOW);
  digitalWrite(R6, HIGH);
  delay(t);
  digitalWrite(R6, LOW);
  digitalWrite(R7, HIGH);
  delay(t);
  digitalWrite(R7, LOW);
  delay(t);
  digitalWrite(R7, HIGH);
  delay(t);
  digitalWrite(R7, LOW);
  digitalWrite(R6, HIGH);
  delay(t);
  digitalWrite(R6, LOW);
  digitalWrite(R5, HIGH);
  delay(t);
  digitalWrite(R5, LOW);
  digitalWrite(R4, HIGH);
  delay(t);
  digitalWrite(R4, LOW);
  digitalWrite(R3, HIGH);
  delay(t);
  digitalWrite(R3, LOW);
  digitalWrite(R2, HIGH);
  delay(t);
  digitalWrite(R2, LOW);
  digitalWrite(R1, HIGH);
  delay(t);
  digitalWrite(R1, LOW);
  delay(t);
}

void loop() {
  // To generate the ultrasound we need to
  // set the trigPin to HIGH state for correct ammount of µs.
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(delay_us);
  digitalWrite(trigPin, LOW);
  
  // Read the pulse HIGH state on echo pin 
  // the length of the pulse in microseconds
  duration_us = pulseIn(echoPin, HIGH);
  
  distance_mm = (duration_us/58/10)*10;
  
  Serial.println(distance_mm);
  vilgu();
  
}

