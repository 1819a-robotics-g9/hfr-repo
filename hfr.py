import numpy as np
import cv2
import time
import gopigo as go
import serial


# Open the camera
cap = cv2.VideoCapture(0)

#for light dots removal parameters
kernel = np.ones((5,5), np.uint8)

# colour detection limits
lH = 40
lS = 45
lV = 68
hH = 86
hS = 255
hV = 220
lowerLimits = np.array([lH, lS, lV])
upperLimits = np.array([hH, hS, hV])

#read Arduino data in
ser = serial.Serial('/dev/ttyUSB0')
ser.flushInput()

#functions to change values
def update_lH(new_value):
    global lH
    global lowerLimits
    lH = new_value
    lowerLimits = np.array([lH, lS, lV])
    return

def update_lS(new_value):
    global lS
    global lowerLimits
    lS = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_lV(new_value):
    global lV
    global lowerLimits
    lV = new_value
    lowerLimits = np.array([lH, lS, lV])
    return


def update_hH(new_value):
    global hH
    global upperLimits
    hH = new_value
    upperLimits = np.array([hH, hS, hV])
    return

def update_hS(new_value):
    global hS
    global upperLimits
    hS = new_value
    upperLimits = np.array([hH, hS, hV])
    return

def update_hV(new_value):
    global hV
    global upperLimits
    hV = new_value
    upperLimits = np.array([hV, hS, hV])
    return

'''
cv2.namedWindow("Trackbars")
cv2.createTrackbar("lH", "Trackbars", lH, 255, update_lH)
cv2.createTrackbar("lS", "Trackbars", lS, 255, update_lS)
cv2.createTrackbar("lV", "Trackbars", lV, 255, update_lV)
cv2.createTrackbar("hH", "Trackbars", hH, 255, update_hH)
cv2.createTrackbar("hS", "Trackbars", hS, 255, update_hS)
cv2.createTrackbar("hV", "Trackbars", hV, 255, update_hV)
'''

blobparams = cv2.SimpleBlobDetector_Params()
blobparams.filterByArea = True
blobparams.maxArea = 90000
blobparams.minArea = 50
blobparams.filterByCircularity = False
blobparams.filterByConvexity = False
blobparams.filterByColor = False


#defining blob detector
detector = cv2.SimpleBlobDetector_create(blobparams)

#Set robot speed range(0-255)
go.set_speed(20)

liikumine = False
kaugus = 0
search = True
t = 0.8
blob_center = 0

def human_close():
    if len(keypoints) == 0:
        search = True
        otsi()
    elif kaugus > 90 and kaugus < 120:
        go.stop()
        print("STOPPAS")
        return True
    else:
        if kaugus < 90:
            go.bwd()
            #time.sleep(t)
            go.stop()
            print("tagasi")
        elif kaugus > 120:
            go.fwd()
            #time.sleep(t)
            #go.stop()
            print("edasi")


def human_center():
    if len(keypoints) == 0:
        otsi()
    else:
        print(blob_center)
        if blob_center <= 300:
            go.right()
            #time.sleep(0.01)
            print(blob_center)
            print("paremale")
            
        if blob_center >= 380:
            go.left()
            #time.sleep(0.01)
            print(blob_center)
            print("vasakule")
    
        #blob_center = 0
    
def otsi():
    go.set_speed(25)
    go.right_rot()
    print("OTSIB")
    if len(keypoints) != 0:
        search = False
    
    
while True:   
    # Read the image from the camera
    ret, frame = cap.read()
    
    r = [len(frame)-170, len(frame)-150, 0, len(frame[0])]
    
    frame=frame[r[0]:r[1], r[2]:r[3]]
    
    #convert bgr to hsv picture
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Thresholded operations
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    thresholded[0] = 255
    thresholded[len(thresholded)-1] = 255
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    outimage = cv2.bitwise_not(outimage)
           
    
    #detected keypoints are there
    keypoints = detector.detect(thresholded)
    
    #make circle around detected item
    #frame = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
      
    #arduino data
    arduino = ser.readline()
    
    cv2.imshow('Filter', thresholded)
 
    #NO BLOBS
    if len(keypoints) < 1 and search == True:
        otsi()

    #BLOB DETECTED
    else:
        search = False
        print("Keypoint leitud")
        print(keypoints)
        if len(keypoints) != 0:
            blob_center = keypoints[0].pt[0]
            
            kaugus = int(arduino)
            print(kaugus)
            go.set_speed(25)
            human_close()
            human_center()
    
    
    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print ('closing program')
go.stop()
cap.release()
cv2.destroyAllWindows()
