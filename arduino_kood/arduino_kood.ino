// Datasheet for Ultrasonic Ranging Module HC - SR04
// https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf

int echoPin = 6;
int trigPin = 5;
int delay_us = 60;  
long distance_mm = 0;
long duration_us = 10;

void setup()  {
  Serial.begin(9600);
  pinMode(echoPin, INPUT);
  pinMode(trigPin, OUTPUT);
 
}

void loop() {
  // To generate the ultrasound we need to
  // set the trigPin to HIGH state for correct ammount of µs.
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(delay_us);
  digitalWrite(trigPin, LOW);
  
  // Read the pulse HIGH state on echo pin 
  // the length of the pulse in microseconds
  duration_us = pulseIn(echoPin, HIGH);
  
  distance_mm = (duration_us/58/10)*10;
  
  Serial.println(distance_mm);
  delay(5000);
}
